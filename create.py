import psycopg2

try:
    connect_str = "dbname='testpython' user='eric138' host='localhost' password='gypsyking138'"

    #use our connection values to establish a connection
    conn = psycopg2.connect(connect_str)

    #create a psycopg2 cursor that can execute queries
    cursor = conn.cursor()

    #create a new table with columns describing the games
    cursor.execute("""CREATE TABLE board_games (Name varchar(255) NOT NULL, Players int NOT NULL, Style varchar(255) NOT NULL, AvgTime int NOT NULL, Location varchar(255) NOT NULL);""")

    #run a select statement - no data in there, but we can try it
    cursor.execute("""SELECT * FROM board_games;""")
    rows = cursor.fetchall()
    print(rows)

    conn.commit()

except Exception as e:
    print("Uh oh, can't connect. Invalid dbname, user, or password?")
    print(e)
