"""
Board Game Collection Suite
By Eric Wideman
12-17-2016

Project includes these features:
    Board Game Collection - Inventory Manager
    Board Game Collection - Game Picker
"""

#imports
import sys
import inventory

#main function of the game.
def start():

    #intro, choose what to do.
    print("Welcome to the Board Game Collection Suite.")
    print()
    print("Enter 'I' for Board Game Collection - Inventory Manager.")
    print("Enter 'P' for Board Game Collection - Game Picker.")
    print("Enter 'X' to exit Board Game Collection Suite.")
    print()

    #get user input and validate input.
    user_input = input("What would you like to do? ")
    user_input = user_input.title()

    #check if input is valid.
    if user_input in ['I', 'P', 'X']:
        if user_input == 'I':
            #TODO go to Inventory Manager.
            print("You typed '" + user_input + "', great!")
            print()
            inventory.start()
        elif user_input == 'P':
            #TODO go to Game Picker.
            print("You typed '" + user_input + "', great!")
            print()
            start()
        else:
            #TODO exit the application.
            sys.exit()
    else:
        print("I'm sorry, that input isn't accepted currently.")
        print("Please try again...")
        print()
        start()
    #restart program.
    start()

#call the main function.
start()
