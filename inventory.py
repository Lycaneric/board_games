"""
Board Game Collection Suite - Inventory Manager
By Eric Wideman
12-19-2016

Inventory Manager:
- Insert new board games into the database
- Edit existing board games in the database
- Remove board games from the database
"""

#imports
import psycopg2

#decide where to send the user.
def start():
    start_active = True
    while start_active:
        print("Welcome to the Inventory Manager!")
        print()
        print("Enter 'A' to ADD a new board game to your collection.")
        print("Enter 'E' to EDIT an existing board game in your collection.")
        print("Enter 'R' to REMOVE an existing board game from your collection.")
        print("Enter 'X' to return to main menu.")
        print()

        #get user input.
        user_input = input("What would you like to do? ")
        user_input = user_input.title()

        #check if input is valid.
        if user_input in ['A', 'E', 'R', 'X']:
            #TODO send to the correct function.
            if user_input == 'A':
                print("You selected " + user_input + ", add a new board game to the collection.")
                print()
                add()
            elif user_input == 'E':
                print("You selected " + user_input + ", edit an existing board game in your collection.")
                print()
                edit()
            elif user_input == 'R':
                print("You selected " + user_input + ", remove an existing board game from your collection.")
                print()
                remove()
            else:
                print("You selected " + user_input + ", leave the Inventory Manager.")
                print()
                start_active = False
                import main
                main.start()
        else:
            print("I'm sorry, that input isn't accepted currently.")
            print("Please try again...")
            print()


#add a board game to your collection.
def add():
    print("Welcome to add.")
    print()
    print("Establishing connection...")
    print()

    try:
        #assign connection string to variable.
        connect_str = "dbname='testpython' user='eric138' host='localhost' password='gypsyking138'"

        #using connection string, establish a connection and assign to variable.
        conn = psycopg2.connect(connect_str)

        #open a cursor using the database connection.
        cursor = conn.cursor()

        print("Connection Established!")
        print()

    except Exception as e:
        print("Uh oh, can't connect. Invalid dbname, user, or password?")
        print(e)
        print()

        #should connection fail, retry or exit.
        conn_loop = True
        while conn_loop:
            print("Enter 'R' to try to connect again.")
            print("Enter 'X' to leave the add function.")
            print()

            #get user input and standardize the input.
            user_input = input("What would you like to do? ")
            user_input = user_input.title()

            #check if input is valid.
            if user_input in ['R', 'X']:
                conn_loop = False
                if user_input == 'R':
                    print("You selected " + user_input + ", try to establish a connection.")
                    print()
                    add()
                else:
                    print("You selected " + user_input + ", exit the add function.")
                    print()
                    return
            else:
                print("I'm sorry, that input isn't accepted currently.")
                print("Please try again...")
                print()

    #get the board game details (Name, Players, Style, AvgTime, Location)
    name = input("What is the name of the board game? ")
    print("You entered, " + name + ", as the name.")
    print()

    players = input("How many players does the game support? ")
    print("You entered, " + players + " players.")
    print()

    style = input("Is the game Co-op(C) or Versus(V)? ")
    style = style.title()
    print("You entered, '" + style + "' for the style.")
    print()

    avg_time = input("How long (in minutes), on average, is the game? ")
    print("You entered, " + avg_time + " minutes, for the average play time.")
    print()

    location = input("Where is the game located? ")
    print("You entered, " + location + ", for where the game is located.")
    print()

    #insert the game into the database.
    insert_loop = True
    while insert_loop:
        print("Your entry is complete.")
        print()
        print("Enter 'A' to add this board game into the database.")
        print("Enter 'C' to cancel the add and start over with the add.")
        print("Enter 'X' to cancel the add and return to the inventory menu.")
        print()

        #get the user input.
        user_input = input("What would you like to do? ")
        user_input = user_input.title()

        #check if input is valid.
        if user_input in ['A', 'C', 'X']:
            insert_loop = False
            if user_input == 'A':
                print("You selected " + user_input + ", add the board game to the database.")
                print()
                #execute the insert statement using the variables defined above.
                cursor.execute("""INSERT INTO board_games VALUES (%s, %s, %s, %s, %s);""", (name, players, style, avg_time, location))
                print("Record added.")
                print()
                #execute a select statement to show the data inserted.
                cursor.execute("""SELECT * FROM board_games WHERE Name = %s;""", (name,))
                #fetch all the rows returned and assign them to rows.
                rows = cursor.fetchall()
                print(rows)
                print()
                #commit_loop.
                commit_loop = True
                while commit_loop:
                    print("Enter 'C' to commit the add.")
                    print("Enter 'X' to cancel the add and return to the inventory menu.")
                    print()
                    #get user input.
                    user_input = input("What would you like to do? ")
                    user_input = user_input.title()
                    #validate the input.
                    if user_input in ['C']:
                        if user_input == 'C':
                            commit_loop = False
                            print("You selected C, commit the add.")
                            print()
                            conn.commit()
                            #TODO add loop back to add another game.
                            print("Transaction committed.")
                            print()
                            return
                        else:
                            print("You selected 'X', return to the inventory menu.")
                            print()
                            return
                    else:
                        print("I'm sorry, that input isn't accepted currently.")
                        print("Please try again...")
                        print()
            elif user_input == 'C':
                print("You selected " + user_input + ", cancel the add and start over.")
                print()
                add()
            else:
                print("You selected " + user_input + ", exit the add function.")
                print()
                return

def edit():
    print("Welcome to edit.")
    print()
    print("Establishing connection...")
    print()

    try:
        #assign connection string to variable.
        connect_str = "dbname='testpython' user='eric138' host='localhost' password='gypsyking138'"

        #using connection string, establish a connection and assign to variable.
        conn = psycopg2.connect(connect_str)

        #open a cursor using the database connection.
        cursor = conn.cursor()

        print("Connection Established!")
        print()

    except Exception as e:
        print("Uh oh, can't connect. Invali dbname, user, or password?")
        print(e)
        print()

        #should connection fail, retry or exit.
        conn_loop = True
        while conn_loop:
            print("Enter 'R' to try to connect again.")
            print("Enter 'X' to leave the edit function.")
            print()

            #get user input and standardize the input.
            user_input = input("What would you like to do? ")
            user_input = user_input.title()

            #check if input is valid.
            if user_input in ['R', 'X']:
                conn_loop = False
                if user_input == 'R':
                    print("You selected " + user_input + ", try to establish a connection.")
                    print()
                    edit()
                else:
                    print("You selected " + user_input + ", exit the edit function.")
                    print()
                    return
            else:
                print("I'm sorry, that input isn't accepted currently.")
                print("Please try again...")
                print()

    #get the name of the game you would like to edit.
    name = input("What is the name of the game to be edited?")
    print("You entered, " + name + ", as the name.")
    print()

    cursor.execute("""SELECT * FROM board_games WHERE name = %s;""", (name,))
    #fetch all the rows returned and assign them to rows.
    rows = cursor.fetchall()
    print(rows)
    print()
    validate_loop = True
    while validate_loop:
        #verify the correct record was selected.
        print("Enter 'Y' if this is the correct game.")
        print("Enter 'N' if this is the incorrect game.")
        print("Enter 'X' to exit the edit function.")
        print()
        user_input = input("Is this the game you want to edit? ")
        user_input = user_input.title()
        if user_input in ['Y', 'N', 'X']:
            validate_loop = False
            if user_input == 'Y':
                print("You selected " + user_input + ", yes this is the correct game to edit.")
                print()
                edit_loop = True
                while edit_loop:
                    print("Enter 'N' to edit the games name.")
                    print("Enter 'P' to edit the number of players.")
                    print("Enter 'S' to edit the games style.")
                    print("Enter 'A' to edit the games average play time.")
                    print("Enter 'L' to edit the location of the game.")
                    print("Enter 'X' to exit the edit function.")
                    print()
                    user_input = input("What would you like to do? ")
                    user_input = user_input.title()
                    #validate the input.
                    if user_input in ['N', 'P', 'S', 'A', 'L']:
                        edit_loop = False
                        input_dict = {
                        'N': 'Name',
                        'P': 'Players',
                        'S': 'Style',
                        'A': 'AvgTime',
                        'L': 'Location'
                        }
                        to_edit = input_dict[user_input]
                        print("You selected to edit the " + to_edit + " field.")
                        print()
                        new_input = input("What do you want the new value to be? ")
                        new_input = new_input.lower()
                        new_input = new_input.title()
                        print("You want the " + to_edit + " field to change to " + new_input + "?")
                        print()
                        validate_loop = True
                        while validate_loop:
                            #verify if this change is correct.
                            print("Enter 'Y' if this change is correct.")
                            print("Enter 'N' if this change is not correct.")
                            print()
                            u_input = input("Is this correct? ")
                            u_input = u_input.title()
                            if u_input in ['Y', 'N']:
                                validate_loop = False
                                if u_input == 'Y':
                                    print("You selected " + u_input + ", yes this change is correct.")
                                    print()
                                    var_list = (to_edit, new_input)
                                    sql = "UPDATE board_games SET {0} = {1} WHERE Name = %%s;".format(var_list) % name
                                    #xlist = (column, table)
                                    #sql = 'select {0} from {1} where utctime > %s and utctime < %s order by utctime asc;'.format(xlist)
                                    cursor.execute(sql)
                                    print("Edit finished.")
                                    print()
                                    rows = cursor.fetchall()
                                    print(rows)
                                    print()
                                    commit_loop = True
                                    while commit_loop:
                                        print("Enter 'Y' if you want to commit this change.")
                                        print("Enter 'N' if you do not want to commit this change.")
                                        print()
                                        user_input = input("What would you like to do? ")
                                        user_input = user_input.title()
                                        print()
                                        if user_input in ['Y', 'N']:
                                            commit_loop = False
                                            if user_input == 'Y':
                                                print("You selected " + user_input + ", yes commit this change.")
                                                print()
                                                conn.commit()
                                                print("Change committed.")
                                                print()
                                                leave_loop = True
                                                while leave_loop:
                                                    print("Enter 'Y' to edit another game.")
                                                    print("Enter 'N' to exit the edit function.")
                                                    print()
                                                    user_input = input("What would you like to do? ")
                                                    user_input = user_input.title()
                                                    print()
                                                    if user_input in ['Y', 'N']:
                                                        leave_loop = False
                                                        if user_input == 'Y':
                                                            print("You selected " + user_input + ", edit another game.")
                                                            print()
                                                            edit()
                                                        else:
                                                            print("You selected " + user_input + ", exit the edit function.")
                                                            print()
                                                            start()
                                                    else:
                                                        print("I'm sorry, that input isn't accepted currently.")
                                                        print("Please try again...")
                                                        print()
                                        else:
                                            print("I'm sorry, that input isn't accepted currently.")
                                            print("Please try again...")
                                            print()
                                else:
                                    print("You selected " + u_input + ", no this change is not correct.")
                                    print()
                                    edit()
                            else:
                                print("I'm sorry, that input isn't accepted currently.")
                                print("Please try again...")
                                print()
                    elif user_input == 'X':
                        print("You selected X, exit the edit function.")
                        print()
                        start()
                    else:
                        print("I'm sorry, that input isn't accepted currently.")
                        print("Please try again...")
                        print()

def remove():
    print("Welcome to remove.")
    print()
    print("Establishing connection...")
    print()

    try:
        #assign connection string to variable.
        connect_str = "dbname='testpython' user='eric138' host='localhost' password='gypsyking138'"

        #using connection string, establish a connection and assign to variable.
        conn = psycopg2.connect(connect_str)

        #open a cursor using the database connection.
        cursor = conn.cursor()

        print("Connection Established!")
        print()

    except Exception as e:
        print("Uh oh, can't connect. Invalid dbname, user, or password?")
        print(e)
        print()

        #should connection fail, retry or exit.
        conn_loop = True
        while conn_loop:
            print("Enter 'R' to try to connect again.")
            print("Enter 'X' to leave the remove function.")
            print()

            #get user input and standardize the input.
            user_input = input("What would you like to do? ")
            user_input = user_input.title()

            #check if input is valid.
            if user_input in ['R', 'X']:
                conn_loop = False
                if user_input == 'R':
                    print("You selected " + user_input + ", try to establish a connection.")
                    print()
                    remove()
                else:
                    print("You selected " + user_input + ", exit the remove function.")
                    print()
                    return
            else:
                print("I'm sorry, that input isn't accepted currently.")
                print("Please try again...")
                print()

    #get the name of the game you would like to remove.
    name = input("What is the name of the board game to be removed?")
    print("You entered, " + name + ", as the name.")
    print()

    cursor.execute("""SELECT * FROM board_games WHERE name = %s;""", (name,))
    #fetch all the rows returned and assign them to rows.
    rows = cursor.fetchall()
    print(rows)
    print()
    #validate the input in validate_loop.
    validate_loop = True
    while validate_loop:
        #verify the correct record was selected.
        print("Enter 'Y' if this is the correct game.")
        print("Enter 'N' if this is the incorrect game.")
        print("Enter 'X' to exit the remove function")
        print()
        user_input = input("Is this the game you want to delete? ")
        user_input = user_input.title()
        if user_input in ['Y', 'N', 'X']:
            if user_input == 'Y':
                print("You selected 'Y', yes this is the correct game to remove.")
                print()
                validate_loop = False
                cursor.execute("""DELETE FROM board_games WHERE name = %s;""", (name,))
                print("The game has been removed.")
                print()
                conn.commit()
                #exit loop
                exit_loop = True
                while exit_loop:
                    print("Enter 'R' to remove another game.")
                    print("Enter 'X' to exit the remove function.")
                    print()
                    user_input = input("What would you like to do? ")
                    user_input = user_input.title()
                    #validate the input.
                    if user_input in ['R', 'X']:
                        if user_input == 'R':
                            exit_loop = False
                            print("You selected 'R' to remove another game.")
                            print()
                            remove()
                        else:
                            exit_loop = False
                            print("You selected 'X' to exit the remove function.")
                            print()
                            start()
                    else:
                        print("I'm sorry, that input isn't accepted currently.")
                        print()
            elif user_input == 'X':
                print("You selected 'X', exit the remove function.")
                print()
                validate_loop == False
                start()
            else:
                print("You selected 'N', no this is the incorrect game to remove.")
                print()
                validate_loop = False
                remove()
        else:
            print("I'm sorry, that input isn't accepted currently.")
            print("Please try again...")
            print()
